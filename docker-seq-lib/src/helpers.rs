use std::fs;
use std::path::Path;

pub fn command_exists(cmd: &str) -> bool {
    if let Ok(paths) = std::env::var("PATH") {
        for path in paths.split(':') {
            let p_str = format!("{path}/{cmd}");
            if std::fs::metadata(p_str).is_ok() {
                return true;
            }
        }
    }
    false
}

pub fn get_local_user_string() -> String {
    #[cfg(target_family = "unix")]
    {
        let uid = users::get_current_uid();
        let gid = users::get_current_gid();
        format!("{uid}:{gid}")
    }
    #[cfg(not(target_family = "unix"))]
    {
        String::from("0:0")
    }
}

pub fn resolve_volume_paths(mut volume_path: String) -> String {
    volume_path = resolve_volume_path_preceeding_tilde(volume_path);
    volume_path = resolve_volume_path_relative_local_path(volume_path);
    volume_path
}

fn resolve_volume_path_preceeding_tilde(mut volume_path: String) -> String {
    if volume_path.starts_with('~') {
        if let Ok(home_dir) = std::env::var("HOME") {
            volume_path = volume_path.replacen('~', &home_dir, 1);
        }
    }
    volume_path
}

fn resolve_volume_path_relative_local_path(volume_path: String) -> String {
    if let Some((local_config, remaining_config)) = volume_path.split_once(':') {
        if let Ok(path) = fs::canonicalize(Path::new(local_config)) {
            if let Ok(path) = path.into_os_string().into_string() {
                return format!("{path}:{remaining_config}");
            }
        }
    }
    volume_path
}
