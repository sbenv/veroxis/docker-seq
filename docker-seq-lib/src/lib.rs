pub mod docker_seq;
pub mod helpers;

use docker_seq::DockerSeq;
use docker_seq::DockerSeqError;

pub fn run(config_json: &str) -> Result<(), DockerSeqError> {
    let docker_seq = DockerSeq::from_json(config_json)?;
    docker_seq.exec()?;
    Ok(())
}
