use std::io;
use std::process::Command;
use std::process::Stdio;
use std::vec;

use color_eyre::owo_colors::OwoColorize;
use scopeguard::defer;
use serde::Deserialize;
use serde::Serialize;
use thiserror::Error;

use crate::helpers::command_exists;
use crate::helpers::get_local_user_string;
use crate::helpers::resolve_volume_paths;

const DOCKER_CMD: &str = "docker";

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(default)]
pub struct DockerSeq {
    pub print_sequence_steps: bool,
    pub verbose: bool,
    pub image: String,
    pub docker_exec_shell: Vec<String>,
    #[serde(default)]
    pub workdir: Option<String>,
    pub user: Option<String>,
    pub local_user: bool,
    pub tty: bool,
    pub interactive: bool,
    pub pull: String,
    pub rm: bool,
    pub init: bool,
    pub volumes: Option<Vec<String>>,
    pub publish: Option<Vec<String>>,
    pub env: Option<Vec<String>>,
    pub env_file: Option<Vec<String>>,
    pub cpus: Option<String>,
    pub memory: Option<u128>,
    pub entrypoint: String,
    pub name: String,
    pub privileged: bool,
    pub network: Option<String>,
    pub mount: Option<Vec<String>>,
    pub read_only: bool,
    pub platform: Option<String>,
    pub runtime: Option<String>,
    pub security_opt: Option<Vec<String>>,
    pub ulimit: Option<String>,
    pub volumes_from: Option<Vec<String>>,
    pub background_task: Vec<String>,
    #[serde(default)]
    pub sequence: Vec<DockerExecStep>,
}

impl Default for DockerSeq {
    fn default() -> Self {
        Self {
            print_sequence_steps: true,
            verbose: false,
            image: String::from("alpine:latest"),
            docker_exec_shell: vec![
                String::from("/usr/bin/env"),
                String::from("sh"),
                String::from("-c"),
            ],
            sequence: Default::default(),
            background_task: vec![
                String::from("/usr/bin/env"),
                String::from("sh"),
                String::from("-c"),
                String::from("sleep infinity"),
            ],
            tty: true,
            interactive: true,
            rm: true,
            init: true,
            entrypoint: String::from(""),
            name: format!("docker-seq_{}", uuid::Uuid::new_v4()),
            workdir: Default::default(),
            user: Default::default(),
            local_user: false,
            pull: String::from("missing"),
            volumes: Default::default(),
            publish: Default::default(),
            env: Default::default(),
            env_file: Default::default(),
            cpus: Default::default(),
            memory: Default::default(),
            privileged: Default::default(),
            network: Default::default(),
            mount: Default::default(),
            read_only: Default::default(),
            platform: Default::default(),
            runtime: Default::default(),
            security_opt: Default::default(),
            ulimit: Default::default(),
            volumes_from: Default::default(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(default)]
pub struct DockerExecStep {
    pub workdir: Option<String>,
    pub user: Option<String>,
    pub local_user: bool,
    pub tty: bool,
    pub privileged: bool,
    pub interactive: bool,
    pub commands: Vec<String>,
}

impl Default for DockerExecStep {
    fn default() -> Self {
        Self {
            workdir: Default::default(),
            user: Default::default(),
            local_user: false,
            tty: true,
            privileged: false,
            interactive: true,
            commands: Default::default(),
        }
    }
}

pub struct DockerExecArgs {
    pub command: String,
    pub args: Vec<String>,
    pub forwarded_command: Vec<String>,
}

#[derive(Debug, Error)]
pub enum DockerSeqError {
    #[error("SerdeYamlError: {0}")]
    SerdeYamlError(#[from] serde_yaml::Error),
    #[error("SerdeJsonError: {0}")]
    SerdeJsonError(#[from] serde_json::Error),
    #[error("ConfigError: {0}")]
    ConfigError(String),
    #[error("CommandExecError: failed to execute `{command}` using args {args:?}: {error}")]
    CommandExecError {
        command: String,
        args: Vec<String>,
        error: io::Error,
    },
    #[error("CommandResultError: received errorcode from `{command}` using args {args:?}")]
    CommandResultError { command: String, args: Vec<String> },
    #[error("CommandNotFoundInPathError: {command}")]
    CommandNotFoundInPathError { command: String },
}

impl DockerSeq {
    pub fn from_yaml(config: &str) -> Result<DockerSeq, DockerSeqError> {
        Ok(serde_yaml::from_str(config)?)
    }

    pub fn to_yaml(&self) -> Result<String, DockerSeqError> {
        Ok(serde_yaml::to_string(self)?)
    }

    pub fn from_json(config: &str) -> Result<DockerSeq, DockerSeqError> {
        Ok(serde_json::from_str(config)?)
    }

    pub fn to_json(&self) -> Result<String, DockerSeqError> {
        Ok(serde_json::to_string(self)?)
    }

    pub fn exec(&self) -> Result<(), DockerSeqError> {
        self.exec_cmd_docker_run()?;
        defer!({
            let _ = self.exec_cmd_docker_stop();
        });
        self.exec_cmd_docker_exec()?;
        Ok(())
    }

    pub fn build_args_docker_run(&self) -> Result<(String, Vec<String>), DockerSeqError> {
        let mut args: Vec<String> = vec!["run".into(), "-d".into()];
        if let Some(workdir) = self.workdir.as_ref() {
            args.append(&mut vec!["--workdir".into(), workdir.into()])
        }
        if self.local_user {
            let user = get_local_user_string();
            self.log_config_msg("user", format!("using local_user `{user}`").as_str());
            args.append(&mut vec!["--user".into(), user]);
        } else if let Some(user) = self.user.as_ref() {
            self.log_config_msg("user", format!("using configured user `{user}`").as_str());
            args.append(&mut vec!["--user".into(), user.into()])
        }
        if self.tty {
            args.append(&mut vec!["--tty".into()]);
        }
        if self.tty {
            args.append(&mut vec!["--interactive".into()]);
        }
        args.append(&mut vec!["--pull".into(), self.pull.to_owned()]);
        if self.rm {
            args.append(&mut vec!["--rm".into()]);
        }
        if self.init {
            args.append(&mut vec!["--init".into()]);
        }
        if let Some(volumes) = self.volumes.as_ref() {
            for volume in volumes.iter() {
                let volume = resolve_volume_paths(volume.to_owned());
                args.append(&mut vec!["--volume".into(), volume]);
            }
        }
        if let Some(publish) = self.publish.as_ref() {
            for p in publish.iter() {
                args.append(&mut vec!["--publish".into(), p.into()]);
            }
        }
        if let Some(env) = self.env.as_ref() {
            for e in env.iter() {
                args.append(&mut vec!["--env".into(), e.into()]);
            }
        }
        if let Some(env_file) = self.env_file.as_ref() {
            for e in env_file.iter() {
                args.append(&mut vec!["--env-file".into(), e.into()]);
            }
        }
        if let Some(cpus) = self.cpus.as_ref() {
            args.append(&mut vec!["--cpus".into(), cpus.into()])
        }
        if let Some(memory) = self.memory.as_ref() {
            args.append(&mut vec!["--memory".into(), memory.to_string()])
        }
        if let Some(memory) = self.memory.as_ref() {
            args.append(&mut vec!["--memory".into(), memory.to_string()])
        }
        args.append(&mut vec!["--name".into(), self.name.to_owned()]);
        if self.privileged {
            args.append(&mut vec!["--privileged".into()]);
        }
        if let Some(network) = self.network.as_ref() {
            args.append(&mut vec!["--network".into(), network.into()])
        }
        if let Some(mount) = self.mount.as_ref() {
            for m in mount.iter() {
                args.append(&mut vec!["--mount".into(), m.into()]);
            }
        }
        if self.read_only {
            args.append(&mut vec!["--read-only".into()]);
        }
        if let Some(platform) = self.platform.as_ref() {
            args.append(&mut vec!["--platform".into(), platform.into()])
        }
        if let Some(runtime) = self.runtime.as_ref() {
            args.append(&mut vec!["--runtime".into(), runtime.into()])
        }
        if let Some(security_opt) = self.security_opt.as_ref() {
            for opt in security_opt.iter() {
                args.append(&mut vec!["--security-opt".into(), opt.into()]);
            }
        }
        if let Some(ulimit) = self.ulimit.as_ref() {
            args.append(&mut vec!["--ulimit".into(), ulimit.into()])
        }
        if let Some(volumes_from) = self.volumes_from.as_ref() {
            for vol in volumes_from.iter() {
                args.append(&mut vec!["--volumes-from".into(), vol.into()]);
            }
        }
        args.append(&mut vec![self.image.to_owned()]);

        for arg in self.background_task.iter() {
            args.append(&mut vec![arg.into()]);
        }
        Ok((DOCKER_CMD.into(), args))
    }

    pub fn exec_cmd_docker_run(&self) -> Result<(), DockerSeqError> {
        let (cmd, args) = self.build_args_docker_run()?;
        if !command_exists(cmd.as_str()) {
            return Err(DockerSeqError::CommandNotFoundInPathError { command: cmd });
        }
        self.log_cmd(cmd.as_str(), &args);
        if self.print_sequence_steps && !self.verbose {
            println!(
                "[{}] {}{}{}",
                "docker_seq".green(),
                "Starting container using image [".green(),
                self.image.yellow(),
                "]".green()
            );
        }
        let status = Command::new(cmd.clone())
            .args(&args)
            .stdin(Stdio::inherit())
            .stderr(Stdio::inherit())
            .stdout(Stdio::null())
            .status()
            .map_err(|error| DockerSeqError::CommandExecError {
                command: cmd.clone(),
                args: args.clone(),
                error,
            })?;
        if !status.success() {
            return Err(DockerSeqError::CommandResultError {
                command: self.name.clone(),
                args,
            });
        }
        Ok(())
    }

    pub fn build_args_docker_exec(&self) -> Result<Vec<DockerExecArgs>, DockerSeqError> {
        let cmd = DOCKER_CMD;
        let mut exec_commands = vec![];
        for step in self.sequence.iter() {
            let mut args = vec!["exec".to_string()];
            if step.tty {
                args.append(&mut vec!["--tty".into()]);
            }
            if step.privileged {
                args.append(&mut vec!["--privileged".into()]);
            }
            if step.interactive {
                args.append(&mut vec!["--interactive".into()]);
            }
            if let Some(workdir) = step.workdir.as_ref() {
                args.append(&mut vec!["--workdir".into(), workdir.clone()]);
            }
            if step.local_user {
                let user = get_local_user_string();
                self.log_config_msg("user", format!("using local_user `{user}`").as_str());
                args.append(&mut vec!["--user".into(), user]);
            } else if let Some(user) = step.user.as_ref() {
                self.log_config_msg("user", format!("using configured user `{user}`").as_str());
                args.append(&mut vec!["--user".into(), user.into()])
            }
            args.append(&mut vec![self.name.clone()]);
            for arg in self.docker_exec_shell.iter() {
                args.append(&mut vec![arg.clone()]);
            }
            for command in step.commands.iter() {
                let mut command_args = args.clone();
                command_args.append(&mut vec![command.clone()]);
                exec_commands.push(DockerExecArgs {
                    command: cmd.to_string(),
                    args: command_args,
                    forwarded_command: vec![command.clone()],
                });
            }
        }
        Ok(exec_commands)
    }

    pub fn exec_cmd_docker_exec(&self) -> Result<(), DockerSeqError> {
        let commands = self.build_args_docker_exec()?;
        if !command_exists(DOCKER_CMD) {
            return Err(DockerSeqError::CommandNotFoundInPathError {
                command: DOCKER_CMD.into(),
            });
        }
        for docker_exec_args in commands.iter() {
            self.log_cmd(docker_exec_args.command.as_str(), &docker_exec_args.args);
            if self.print_sequence_steps && !self.verbose {
                println!(
                    "[{}] {} {}",
                    "docker_seq".green(),
                    ">_".blue(),
                    docker_exec_args.forwarded_command.join(" ").purple()
                );
            }
            let status = Command::new(docker_exec_args.command.clone())
                .args(&docker_exec_args.args)
                .status()
                .map_err(|error| DockerSeqError::CommandExecError {
                    command: docker_exec_args.command.to_string(),
                    args: docker_exec_args.args.clone(),
                    error,
                })?;
            if !status.success() {
                return Err(DockerSeqError::CommandResultError {
                    command: docker_exec_args.command.clone(),
                    args: docker_exec_args.args.clone(),
                });
            }
        }
        Ok(())
    }

    pub fn exec_cmd_docker_stop(&self) -> Result<(), DockerSeqError> {
        let cmd = DOCKER_CMD;
        if !command_exists(cmd) {
            return Err(DockerSeqError::CommandNotFoundInPathError {
                command: cmd.into(),
            });
        }
        let args: Vec<String> = vec!["stop".into(), "-t".into(), "0".into(), self.name.clone()];
        self.log_cmd(cmd, &args);
        if self.print_sequence_steps && !self.verbose {
            println!("[{}] {}", "docker_seq".green(), "Shutting down".green());
        }
        let status = Command::new(cmd)
            .args(&args)
            .stderr(Stdio::inherit())
            .stdin(Stdio::inherit())
            .stdout(Stdio::null())
            .status()
            .map_err(|error| DockerSeqError::CommandExecError {
                command: cmd.to_string(),
                args: args.clone(),
                error,
            })?;
        if !status.success() {
            return Err(DockerSeqError::CommandResultError {
                command: String::from(cmd),
                args,
            });
        }
        Ok(())
    }

    fn log_config_msg(&self, config_type: &str, msg: &str) {
        if self.verbose {
            println!(
                "[{}] [{}] {}",
                "docker_seq".green(),
                config_type.blue(),
                msg.yellow()
            );
        }
    }

    fn log_cmd(&self, command: &str, args: &[String]) {
        if self.verbose {
            println!(
                "[{}] {} {} {}",
                "docker_seq".green(),
                ">_".blue(),
                command.purple(),
                args.join(" ").purple()
            );
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default_cmd_works() {
        let config = DockerSeq::default();
        let (cmd, args) = config.build_args_docker_run().unwrap();
        assert_eq!(cmd, "docker");
        assert!(
            args.contains(&"run".into()),
            "the parameter `run` should be set"
        );
        assert!(
            args.contains(&"-d".into()),
            "the parameter `-d` should be set"
        );
        assert!(
            args.contains(&"--tty".into()),
            "the parameter `--tty` should be set"
        );
        assert!(
            args.contains(&"--interactive".into()),
            "the parameter `--interactive` should be set"
        );
        assert!(
            args.contains(&"--pull".into()),
            "the parameter `--pull` should be set"
        );
        assert!(
            args.contains(&"missing".into()),
            "the argument `missing` should be set"
        );
        assert!(
            args.contains(&"--rm".into()),
            "the parameter `--rm` should be set"
        );
        assert!(
            args.contains(&"--init".into()),
            "the parameter `--init` should be set"
        );
        assert!(
            args.contains(&"--name".into()),
            "the parameter `--name` should be set"
        );
        assert!(
            args.contains(&"alpine:latest".into()),
            "the default image `alpine:latest` should be set"
        );
        assert!(
            args.contains(&"/usr/bin/env".into()),
            "the shell should be called through `/usr/bin/env`"
        );
        assert!(
            args.contains(&"sh".into()),
            "the default shell to run in should be `sh`"
        );
        assert!(
            args.contains(&"-c".into()),
            "the shell argument `-c` should be set"
        );
        assert!(
            args.contains(&"sleep infinity".into()),
            "the default command for keeping the container alive should be set to `sleep infinity`"
        );
        assert_eq!(
            args.iter()
                .filter(|arg| arg.starts_with("docker-seq_"))
                .count(),
            1,
            "a single field for the container name starting with `docker-seq_` should exist"
        );
        assert_eq!(
            args.len(),
            15,
            "the default argument list should contain 15 arguments"
        );
    }
}
