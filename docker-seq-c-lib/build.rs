extern crate cbindgen;

use std::env;

use cbindgen::Language;

fn main() {
    let crate_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let target_dir = env::var("OUT_DIR").unwrap();

    cbindgen::Builder::new()
        .with_crate(&crate_dir)
        .with_language(Language::C)
        .generate()
        .expect("Unable to generate c bindings")
        .write_to_file(format!("{target_dir}/../../../dockerseq.h"));

    cbindgen::Builder::new()
        .with_crate(&crate_dir)
        .with_language(Language::Cxx)
        .generate()
        .expect("Unable to generate cxx bindings")
        .write_to_file(format!("{target_dir}/../../../dockerseq.hpp"));

    cbindgen::Builder::new()
        .with_crate(&crate_dir)
        .with_language(Language::Cython)
        .generate()
        .expect("Unable to generate pxd bindings")
        .write_to_file(format!("{target_dir}/../../../dockerseq.pxd"));
}
