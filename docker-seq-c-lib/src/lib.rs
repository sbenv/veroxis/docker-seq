use std::ffi::CStr;
use std::ffi::CString;
use std::os::raw::c_char;

/// Execute docker-seq using the given configuration in json format
///
/// If an error happend, it is returned as an error message.
#[no_mangle]
pub extern "C" fn DS_run(config_json: *const c_char) -> *const c_char {
    let config_json = c_char_to_string(config_json).unwrap();
    let result = docker_seq_lib::run(config_json.as_str());
    match result {
        Ok(_) => std::ptr::null(),
        Err(err) => str_to_cstring(err.to_string().as_str()).into_raw(),
    }
}

#[no_mangle]
/// Reclaims and cleans up the a string after passing it through ffi.
///
/// Has to be called from the other side.
///
/// # Safety
///
/// Not safe. Handle with care.
pub unsafe extern "C" fn DS_cleanup_string(result: *mut c_char) {
    if !result.is_null() {
        let _ = CString::from_raw(result);
    }
}

fn str_to_cstring(string: &str) -> CString {
    CString::new(string.as_bytes()).unwrap()
}

fn c_char_to_string(string: *const c_char) -> Option<String> {
    match string.is_null() {
        true => None,
        false => Some(
            unsafe { CStr::from_ptr(string) }
                .to_str()
                .expect("no utf8?!")
                .to_string(),
        ),
    }
}
