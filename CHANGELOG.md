## [1.6.3](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.6.2...v1.6.3) (2023-07-02)


### Bug Fixes

* downgrade crate tracing and cbindgen ([c1e7f4b](https://gitlab.com/sbenv/veroxis/docker-seq/commit/c1e7f4bd935b7d9690fdf2a25cca9bbacb45acde))

## [1.6.2](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.6.1...v1.6.2) (2023-01-28)


### Bug Fixes

* upgrade to rust 1.67 ([ec2f8b9](https://gitlab.com/sbenv/veroxis/docker-seq/commit/ec2f8b9a891bdcfa604be7b35fa6425ced0c30db))

## [1.6.1](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.6.0...v1.6.1) (2023-01-08)


### Bug Fixes

* update peer-dependencies ([dffd207](https://gitlab.com/sbenv/veroxis/docker-seq/commit/dffd207216b25b4966970bc79ba80ad14a95a4ad))

# [1.6.0](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.5.1...v1.6.0) (2022-12-21)


### Features

* allow compiling on non-unix systems through fallbacks ([142db01](https://gitlab.com/sbenv/veroxis/docker-seq/commit/142db0193900b4abedeaa50dfe329c510c2598e9))

## [1.5.1](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.5.0...v1.5.1) (2022-12-16)


### Bug Fixes

* clippy issues with rust 1.66.0 ([3275446](https://gitlab.com/sbenv/veroxis/docker-seq/commit/3275446c3425182b31430be1ed994511f3e87660))

# [1.5.0](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.4.9...v1.5.0) (2022-07-12)


### Features

* **docker-volume:** resolve `~` as `${HOME}` ([e3b127e](https://gitlab.com/sbenv/veroxis/docker-seq/commit/e3b127e38aa09ea0364be727442bb98969493fa3))

## [1.4.9](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.4.8...v1.4.9) (2022-06-15)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.5 ([b6f066f](https://gitlab.com/sbenv/veroxis/docker-seq/commit/b6f066f09300d363f9c3fdafa05ec94e35f21745))

## [1.4.8](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.4.7...v1.4.8) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.4 ([328a8f1](https://gitlab.com/sbenv/veroxis/docker-seq/commit/328a8f11726ffda71fc762188bc33259522e43ff))

## [1.4.7](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.4.6...v1.4.7) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.3 ([f8986a3](https://gitlab.com/sbenv/veroxis/docker-seq/commit/f8986a38203026674b46e297c1c3660ae31f3754))

## [1.4.6](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.4.5...v1.4.6) (2022-06-14)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.2 ([8a11923](https://gitlab.com/sbenv/veroxis/docker-seq/commit/8a11923b2a1b951e8397b43524331f823ec3a380))

## [1.4.5](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.4.4...v1.4.5) (2022-06-13)


### Bug Fixes

* **deps:** update rust crate clap to 3.2.1 ([1dce1c2](https://gitlab.com/sbenv/veroxis/docker-seq/commit/1dce1c2c31da55feb937729156ecf5002f6f3c6d))
* use clap's new value_parser attribute ([c418023](https://gitlab.com/sbenv/veroxis/docker-seq/commit/c418023a13001bbef0265f1332ec79a4da4e886c))

## [1.4.4](https://gitlab.com/sbenv/veroxis/docker-seq/compare/v1.4.3...v1.4.4) (2022-06-10)


### Bug Fixes

* **deps:** update rust crate uuid to 1.1.2 ([3cf0dfe](https://gitlab.com/sbenv/veroxis/docker-seq/commit/3cf0dfee324371ed2340b1f7d7bdce60cfb667ea))

## [1.4.3](https://gitlab.com/Veroxis/docker-seq/compare/v1.4.2...v1.4.3) (2022-06-08)


### Bug Fixes

* **deps:** update rust crate tracing to 0.1.35 ([b834f51](https://gitlab.com/Veroxis/docker-seq/commit/b834f514517e0f852279c168e33251d109f72c14))

## [1.4.2](https://gitlab.com/Veroxis/docker-seq/compare/v1.4.1...v1.4.2) (2022-06-01)


### Bug Fixes

* **deps:** update rust crate uuid to 1.1.1 ([c1b46ef](https://gitlab.com/Veroxis/docker-seq/commit/c1b46ef1822ac692a8e17f858c08d13bf3784a00))

## [1.4.1](https://gitlab.com/Veroxis/docker-seq/compare/v1.4.0...v1.4.1) (2022-05-28)


### Bug Fixes

* **version:** set correct version manually ([4d1e3da](https://gitlab.com/Veroxis/docker-seq/commit/4d1e3dab9b7393ca00a9acc2f37701cb83835c01))

# [1.4.0](https://gitlab.com/Veroxis/docker-seq/compare/v1.3.2...v1.4.0) (2022-05-28)


### Bug Fixes

* **ci:** add workspace flag ([96d6698](https://gitlab.com/Veroxis/docker-seq/commit/96d6698b4ef559f49d4999e9ed4edee9e4eeac24))


### Features

* switch from the deprecated `structopt` cli library to `clap` ([f9a58ad](https://gitlab.com/Veroxis/docker-seq/commit/f9a58ad86fcca592716e0baed9bcf201b72cf5df))

## [1.3.2](https://gitlab.com/Veroxis/docker-seq/compare/v1.3.1...v1.3.2) (2022-05-22)


### Bug Fixes

* **updates:** updated dependencies ([7757248](https://gitlab.com/Veroxis/docker-seq/commit/7757248d759e1f5bdc6c747d9ac1cbbb124662ed))

## [1.3.1](https://gitlab.com/Veroxis/docker-seq/compare/v1.3.0...v1.3.1) (2022-02-22)


### Bug Fixes

* fixed printing forwarded commands ([dbc955a](https://gitlab.com/Veroxis/docker-seq/commit/dbc955a03ae7d465d1c33106062ec404dd9088da))

# [1.3.0](https://gitlab.com/Veroxis/docker-seq/compare/v1.2.0...v1.3.0) (2022-02-18)


### Features

* added `build/native` command as default in `Makefile` ([60e2990](https://gitlab.com/Veroxis/docker-seq/commit/60e2990dab22ffa5ab575c510c572b33ead9738a))
* added an more specific error for commands which aren't installed `IoError` -> `CommandNotFoundInPathError` ([ace4b09](https://gitlab.com/Veroxis/docker-seq/commit/ace4b095523e6c474958c1b98583df301a79efd4))

# [1.2.0](https://gitlab.com/Veroxis/docker-seq/compare/v1.1.0...v1.2.0) (2022-02-18)


### Features

* improved verbosity of error ([7f89273](https://gitlab.com/Veroxis/docker-seq/commit/7f8927310edc1ede7226419ef5036c4901deb769))

# [1.1.0](https://gitlab.com/Veroxis/docker-seq/compare/v1.0.0...v1.1.0) (2022-02-17)


### Features

* added an more specific error for commands which aren't installed `IoError` -> `CommandNotFoundInPathError` ([2709b06](https://gitlab.com/Veroxis/docker-seq/commit/2709b0688d7a41842df97bb4f0c2dc194186be68))

# 1.0.0 (2022-02-13)


### Bug Fixes

* removed `Cargo.lock` from `.gitignore` ([0129fe2](https://gitlab.com/Veroxis/docker-seq/commit/0129fe2999163be241b4afc517136ff884d90c0f))
* removed `default` declarations ([a61fc68](https://gitlab.com/Veroxis/docker-seq/commit/a61fc68e17a24edb047f3d0a7fff7452ecb5b785))
* updated `volume` -> `volumes` config ([e299d00](https://gitlab.com/Veroxis/docker-seq/commit/e299d003d4402b8a7753b5aa71ac27265084dff5))


### Features

* added `.gitlab-ci.yml` ([ad30bed](https://gitlab.com/Veroxis/docker-seq/commit/ad30bed5a9890ca13e3a3b1ba42696851ab69ead))
* added `docker-seq.schema.json` ([f62512d](https://gitlab.com/Veroxis/docker-seq/commit/f62512dd57c6ad5052076651242705970b80109d))
* added c-ffi bindings ([a8d654c](https://gitlab.com/Veroxis/docker-seq/commit/a8d654cf5865d55412edd7315054f32c34225334))
* added debug output for config deserialization ([f10b681](https://gitlab.com/Veroxis/docker-seq/commit/f10b6814335ca6e57dbdf53d3b8e4f9f72ca6682))
* added null check ([f6c14f4](https://gitlab.com/Veroxis/docker-seq/commit/f6c14f4c84a08b11b2283c129cbdf24390cc1adc))
* added semantic-release ([7613670](https://gitlab.com/Veroxis/docker-seq/commit/7613670b6bbad5f70509700c839d560b68c60f07))
* migrated to the `ezd:docker_seq` toolchain ([eefcfe9](https://gitlab.com/Veroxis/docker-seq/commit/eefcfe93d17123e6351dd792d833d37975d5ba6c))
* moved repository `github.com/remissio` -> `gitlab.com/veroxis` ([76597ea](https://gitlab.com/Veroxis/docker-seq/commit/76597eaa6e6bfb31b36302a432b8c7bd5375f8b7))
* new features ([f8d479a](https://gitlab.com/Veroxis/docker-seq/commit/f8d479a4115fa590bb7dd3277435f766cd173306))
* removed debug-build release ([98df4ed](https://gitlab.com/Veroxis/docker-seq/commit/98df4ed545addb4fb2ef58d8de4a1ff94454839c))
* renamed `volume` -> `volumes` ([bffb1d2](https://gitlab.com/Veroxis/docker-seq/commit/bffb1d2d4ec863600c02120d71436f987f4d6ebc))
