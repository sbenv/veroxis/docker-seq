/// A docker plugin requires a subcommand
///
/// This returns a json string containing metadata
pub fn hook() {
    println!(
        "{}",
        serde_json::json!({
            "SchemaVersion": "0.1.0",
            "Vendor": "https://gitlab.com/veroxis",
            "Version": env!("CARGO_PKG_VERSION"),
            "ShortDescription": "Docker Sequence"
        })
    );
}
