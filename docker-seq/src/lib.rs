use std::fs::read_to_string;
use std::io;

use clap::Parser;
use color_eyre::owo_colors::OwoColorize;
use docker_seq_lib::docker_seq::DockerSeq;
use scopeguard::defer;
use thiserror::Error;
use tracing::debug;
use tracing_subscriber::EnvFilter;
mod docker_plugin;

#[derive(Debug, Parser)]
#[clap()]
pub struct AppArgs {
    #[clap(subcommand)]
    pub command: Commands,
}

#[derive(Debug, Parser)]
#[clap()]
pub enum Commands {
    /// execute docker-seq
    #[clap(name = "seq")]
    Seq(Box<DockerSeqArgs>),

    /// metadata command, required for docker-plugins
    #[clap(name = "docker-cli-plugin-metadata")]
    DockerCliPluginMetadata,
}

#[derive(Debug, Parser)]
#[clap()]
pub struct DockerSeqArgs {
    /// which config file to load
    #[clap(
        short,
        long,
        env = "DS_FILE",
        default_value = "docker-seq.yml",
        required_if_eq("config", ""),
        value_parser
    )]
    pub file: String,

    /// pass in a config string instead of a file path
    #[clap(short, long, env = "DS_CONFIG", default_value = "", value_parser)]
    pub config: String,

    /// enable verbose output
    #[clap(short, long, value_parser)]
    pub verbose: bool,

    /// hide executed step commands
    #[clap(long, value_parser)]
    pub hide_commands: bool,

    /// display the parsed config and exit
    #[clap(short, long, value_parser)]
    pub describe: bool,

    /// when using `--describe`, output in json
    #[clap(short, long, value_parser)]
    pub json: bool,

    /// configure the shell args to use for executing commands within the container
    #[clap(long, env = "DS_DOCKER_EXEC_SHELL", value_parser)]
    pub docker_exec_shell: Option<Vec<String>>,

    /// Task that will be executed in the background by `docker run`
    #[clap(long, env = "DS_BACKGROUND_TASK", value_parser)]
    pub background_task: Option<Vec<String>>,

    /// IMAGE
    #[clap(long, env = "DS_IMAGE", value_parser)]
    pub image: Option<String>,

    /// Working directory inside the container
    #[clap(long, env = "DS_WORKDIR", value_parser)]
    pub workdir: Option<String>,

    /// Username or UID (format: <name|uid>[:<group|gid>])
    #[clap(long, env = "DS_USER", value_parser)]
    pub user: Option<String>,

    /// Allocate a pseudo-TTY
    #[clap(long, env = "DS_TTY", value_parser)]
    pub tty: Option<bool>,

    /// Keep STDIN open even if not attached
    #[clap(long, env = "DS_INTERACTIVE", value_parser)]
    pub interactive: Option<bool>,

    /// Pull image before running ("always"|"missing"|"never") (default "missing")
    #[clap(long, env = "DS_PULL", value_parser)]
    pub pull: Option<String>,

    /// Automatically remove the container when it exits
    #[clap(long, env = "DS_RM", value_parser)]
    pub rm: Option<bool>,

    /// Run an init inside the container that forwards signals and reaps processes
    #[clap(long, env = "DS_INIT", value_parser)]
    pub init: Option<bool>,

    /// Bind mount a volume
    #[clap(long, env = "DS_VOLUME", value_parser)]
    pub volume: Option<Vec<String>>,

    /// Publish a container's port(s) to the host
    #[clap(long, env = "DS_PUBLISH", value_parser)]
    pub publish: Option<Vec<String>>,

    /// Set environment variables
    #[clap(long, env = "DS_ENV", value_parser)]
    pub env: Option<Vec<String>>,

    /// Read in a file of environment variables
    #[clap(long, env = "DS_ENV_FILE", value_parser)]
    pub env_file: Option<Vec<String>>,

    /// Number of CPUs
    #[clap(long, env = "DS_CPUS", value_parser)]
    pub cpus: Option<String>,

    /// Memory limit
    #[clap(long, env = "DS_MEMORY", value_parser)]
    pub memory: Option<u128>,

    /// Overwrite the default ENTRYPOINT of the image
    #[clap(long, env = "DS_ENTRYPOINT", value_parser)]
    pub entrypoint: Option<String>,

    /// Username or UID (format: <name|uid>[:<group|gid>])
    #[clap(long, env = "DS_NAME", value_parser)]
    pub name: Option<String>,

    /// Give extended privileges to this container
    #[clap(long, env = "DS_PRIVILEGED", value_parser)]
    pub privileged: Option<bool>,

    /// Connect a container to a network
    #[clap(long, env = "DS_NETWORK", value_parser)]
    pub network: Option<String>,

    /// Attach a filesystem mount to the container
    #[clap(long, env = "DS_MOUNT", value_parser)]
    pub mount: Option<Vec<String>>,

    /// Mount the container's root filesystem as read only
    #[clap(long, env = "DS_READ_ONLY", value_parser)]
    pub read_only: Option<bool>,

    /// Set platform if server is multi-platform capable
    #[clap(long, env = "DS_PLATFORM", value_parser)]
    pub platform: Option<String>,

    /// Runtime to use for this container
    #[clap(long, env = "DS_RUNTIME", value_parser)]
    pub runtime: Option<String>,

    /// Security Options
    #[clap(long, env = "DS_SECURITY_OPT", value_parser)]
    pub security_opt: Option<Vec<String>>,

    /// Ulimit options (default [])
    #[clap(long, env = "DS_ULIMIT", value_parser)]
    pub ulimit: Option<String>,

    /// Mount volumes from the specified container(s)
    #[clap(long, env = "DS_VOLUMES_FROM", value_parser)]
    pub volumes_from: Option<Vec<String>>,
}

#[derive(Debug, Error)]
pub enum DockerSeqError {
    #[error("EyreError: {0}")]
    ColorEyreError(#[from] color_eyre::eyre::Report),
    #[error("ConfigNotFound: path: {path} error: {error}")]
    ConfigNotFound { path: String, error: io::Error },
    #[error("DockerSeqLibError: {0}")]
    DockerSeqLibError(#[from] docker_seq_lib::docker_seq::DockerSeqError),
    #[error("ConfigDeserializationError")]
    ConfigDeserializationError,
}

pub fn run() -> Result<(), DockerSeqError> {
    init()?;
    match AppArgs::parse().command {
        Commands::Seq(args) => cmd_seq(*args),
        Commands::DockerCliPluginMetadata => {
            docker_plugin::hook();
            Ok(())
        }
    }
}

fn init() -> Result<(), DockerSeqError> {
    #[cfg(debug_assertions)]
    {
        if std::env::var("RUST_LIB_BACKTRACE").is_err() {
            std::env::set_var("RUST_LIB_BACKTRACE", "1")
        }
        if std::env::var("RUST_BACKTRACE").is_err() {
            std::env::set_var("RUST_BACKTRACE", "full")
        }
    }

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info");
    }

    color_eyre::install()?;

    tracing_subscriber::fmt::fmt()
        .with_target(false)
        .with_level(true)
        .with_env_filter(EnvFilter::from_default_env())
        .init();
    Ok(())
}

fn cmd_seq(args: DockerSeqArgs) -> Result<(), DockerSeqError> {
    let config = if args.config.is_empty() {
        debug!("Using config file: `{}`", args.file.as_str());
        match read_to_string(args.file.as_str()) {
            Ok(config) => config,
            Err(err) => {
                return Err(DockerSeqError::ConfigNotFound {
                    path: args.file,
                    error: err,
                });
            }
        }
    } else {
        debug!("Using config string: `{}`", args.file.as_str());
        String::clone(&args.config)
    };
    let mut config = load_config_file(&config)?;
    apply_args_to_config(&args, &mut config);

    if args.describe {
        let config_str = match args.json {
            true => config.to_json()?,
            false => config.to_yaml()?,
        };
        println!("{config_str}");
        return Ok(());
    }

    debug!("Starting Container `{}`", config.name.yellow());
    config.exec_cmd_docker_run()?;

    defer!({
        debug!("Stopping Container `{}`", config.name.yellow());
        let _ = config.exec_cmd_docker_stop();
    });

    debug!("Executing Sequence in Container `{}`", config.name.yellow());
    config.exec_cmd_docker_exec()?;

    Ok(())
}

fn load_config_file(file_content: &str) -> Result<DockerSeq, DockerSeqError> {
    match DockerSeq::from_yaml(file_content) {
        Ok(config) => return Ok(config),
        Err(err) => debug!(
            "the config can't be read through `yaml` deserialization: {}",
            err
        ),
    };
    match DockerSeq::from_json(file_content) {
        Ok(config) => return Ok(config),
        Err(err) => debug!(
            "the config can't be read through `json` deserialization: {}",
            err
        ),
    };
    Err(DockerSeqError::ConfigDeserializationError)
}

fn apply_args_to_config(args: &DockerSeqArgs, config: &mut DockerSeq) {
    config.verbose = args.verbose;
    config.print_sequence_steps = !args.hide_commands;
    if let Some(image) = &args.image {
        config.image = image.clone();
    }
    if let Some(workdir) = &args.workdir {
        config.workdir = Some(workdir.clone());
    }
    if let Some(user) = &args.user {
        config.user = Some(user.clone());
    }
    if let Some(tty) = &args.tty {
        config.tty = *tty;
    }
    if let Some(interactive) = &args.interactive {
        config.interactive = *interactive;
    }
    if let Some(pull) = &args.pull {
        config.pull = pull.clone();
    }
    if let Some(rm) = &args.rm {
        config.rm = *rm;
    }
    if let Some(init) = &args.init {
        config.init = *init;
    }
    if let Some(volume) = &args.volume {
        config.volumes = Some(volume.clone());
    }
    if let Some(publish) = &args.publish {
        config.publish = Some(publish.clone());
    }
    if let Some(env) = &args.env {
        config.env = Some(env.clone());
    }
    if let Some(env_file) = &args.env_file {
        config.env_file = Some(env_file.clone());
    }
    if let Some(cpus) = &args.cpus {
        config.cpus = Some(cpus.clone());
    }
    if let Some(memory) = &args.memory {
        config.memory = Some(*memory);
    }
    if let Some(entrypoint) = &args.entrypoint {
        config.entrypoint = entrypoint.clone();
    }
    if let Some(name) = &args.name {
        config.name = name.clone();
    }
    if let Some(privileged) = &args.privileged {
        config.privileged = *privileged;
    }
    if let Some(network) = &args.network {
        config.network = Some(network.clone());
    }
    if let Some(mount) = &args.mount {
        config.mount = Some(mount.clone());
    }
    if let Some(read_only) = &args.read_only {
        config.read_only = *read_only;
    }
    if let Some(platform) = &args.platform {
        config.platform = Some(platform.clone());
    }
    if let Some(runtime) = &args.runtime {
        config.runtime = Some(runtime.clone());
    }
    if let Some(security_opt) = &args.security_opt {
        config.security_opt = Some(security_opt.clone());
    }
    if let Some(ulimit) = &args.ulimit {
        config.ulimit = Some(ulimit.clone());
    }
    if let Some(volumes_from) = &args.volumes_from {
        config.volumes_from = Some(volumes_from.clone());
    }
}
